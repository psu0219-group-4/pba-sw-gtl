﻿using gtl_backend.Controllers;
using gtl_backend.Managers;
using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories;
using gtl_backend.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace test_gtl_backend
{
    public class LoanControllerTests
    {
        [Fact]
        public void CreateLoan_Success()
        {
            // Arrange
            string ssn = "123456789";
            int id = 1;

            var loanList = new List<Loan>();
            var repo = new Mock<ILoanRepository>();
            repo.Setup(r => r.Add(It.IsAny<Loan>())).Callback<Loan>(loan => { loanList.Add(loan); }).Returns(1);
            var manager = new LoanManager(repo.Object);
            var ctrl = new LoanController(manager);

            // Act
            var result = ctrl.CreateLoan(id, ssn);

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.Equal(loanList.First().BookId, id);
        }

        [Fact]
        public void CreateLoan_Failed_ReturnsBadRequest()
        {
            // Arrange
            string ssn = "123456789";
            int id = 1;

            var repo = new Mock<ILoanRepository>();
            repo.Setup(r => r.Add(It.IsAny<Loan>())).Returns(0);
            var manager = new LoanManager(repo.Object);
            var ctrl = new LoanController(manager);

            // Act
            var result = ctrl.CreateLoan(id, ssn);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal("Error!", ((BadRequestObjectResult)result).Value);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void CreateLoanTest_StringEmptyOrNull_ThrowsException(string ssn)
        {
            // Arrange
            int id = 1;

            var manager = new Mock<ILoanManager>();
            var ctrl = new LoanController(manager.Object);

            // Act
            var action = new Action(() => ctrl.CreateLoan(id, ssn));

            // Assert
            Assert.Throws<ArgumentNullException>(action);
        }

        [Fact]
        public void CreateLoanTest_IdIsNegative_ThrowsException()
        {
            // Arrange
            string ssn = "HELLO";
            int id = -1;

            var manager = new Mock<ILoanManager>();
            var ctrl = new LoanController(manager.Object);

            // Act
            var action = new Action(() => ctrl.CreateLoan(id, ssn));

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(action);
        }

        [Fact]
        public void CreateLoan_Failed_TooManyLoans()
        {
            // Arrange
            string ssn = "123456789";
            int id = 1;

            var loanList = new List<Loan>();

            var repo = new Mock<ILoanRepository>();
            repo.Setup(r => r.GetActiveLoans(It.IsAny<string>())).Returns(new List<Loan> { new Loan(), new Loan(), new Loan(), new Loan(), new Loan() });
            repo.Setup(r => r.Add(It.IsAny<Loan>())).Callback<Loan>(loan => { loanList.Add(loan); }).Returns(0);

            var manager = new Mock<ILoanManager>();
            manager.Setup(m => m.LoanBook(It.IsAny<int>(), It.IsAny<string>()))
                .Callback<int, string>((bookId, loanerSsn) => 
                {
                    var activeLoans = repo.Object.GetActiveLoans(loanerSsn);
                    if (activeLoans.Count >= 5) throw new InvalidOperationException();

                    var loan = new Loan
                    {
                        BookId = bookId,
                        MemberSsn = loanerSsn
                    };

                    repo.Object.Add(loan);
                }).Returns(0);

            var ctrl = new LoanController(manager.Object);

            // Act
            var result = ctrl.CreateLoan(id, ssn);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

    }
}
