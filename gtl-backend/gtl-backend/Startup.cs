﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers;
using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories;
using gtl_backend.Repositories.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace gtl_backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            BuildAppSettingsProvider();
        }

        private void BuildAppSettingsProvider()
        {
            AppSettingsProvider.ConnectionString = Configuration.GetSection("DatabaseConfig:ConnectionString").Value;
            AppSettingsProvider.ProviderName = Configuration.GetSection("DatabaseConfig:ProviderName").Value;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add Singletons here:


            // Singleton Managers:
            services.AddTransient<IAuthorManager, AuthorManager>();
            services.AddTransient<IBookCopyManager, BookCopyManager>();
            services.AddTransient<IBookManager, BookManager>();
            services.AddTransient<ILoanManager, LoanManager>();
            services.AddTransient<IMapManager, MapManager>();
            services.AddTransient<IProfessorManager, ProfessorManager>();
            services.AddTransient<IStudentManager, StudentManager>();

            // Singleton Repositories:
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IBookCopyRepository, BookCopyRepository>();
            services.AddTransient<IBookRepository, BookRepository>();
            services.AddTransient<ILoanRepository, LoanRepository>();
            services.AddTransient<IMapRepository, MapRepository>();
            services.AddTransient<IProfessorRepository, ProfessorRepository>();
            services.AddTransient<IStudentRepository, StudentRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
