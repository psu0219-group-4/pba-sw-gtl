﻿using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;

namespace gtl_backend.Repositories
{
    public class LoanRepository : ILoanRepository
    {
        public int Add(Loan entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_CreateLoan";

                    command.CommandType = CommandType.StoredProcedure;

                    var ssnParam = new SqlParameter("@SSN", SqlDbType.NVarChar, 10);
                    ssnParam.Value = entity.MemberSsn;

                    var bookCopyIdParam = new SqlParameter("@BookCopyId", SqlDbType.Int);
                    bookCopyIdParam.Value = entity.BookId;

                    command.Parameters.Add(ssnParam);
                    command.Parameters.Add(bookCopyIdParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error handling.
                throw ex;
            }
        }

        public int Delete(Loan entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_ReturnLoan";

                    command.CommandType = CommandType.StoredProcedure;

                    var ssnParam = new SqlParameter("@SSN", SqlDbType.NVarChar, 10);
                    ssnParam.Value = entity.MemberSsn;

                    var bookCopyIdParam = new SqlParameter("@BookCopyId", SqlDbType.Int);
                    bookCopyIdParam.Value = entity.BookId;

                    command.Parameters.Add(ssnParam);
                    command.Parameters.Add(bookCopyIdParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error Handling
                throw ex;
            }
        }

        public List<Loan> GetActiveLoans(string ssn)
        {
            throw new NotImplementedException();
        }

        public Loan GetByInt(int id)
        {
            throw new NotImplementedException();
        }

        public Loan GetByString(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Loan entity)
        {
            throw new NotImplementedException();
        }
    }
}
