﻿using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories
{
    public class ProfessorRepository : IProfessorRepository
    {
        public int Add(Professor entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_CreateProfessor";

                    command.CommandType = CommandType.StoredProcedure;

                    var ssnParam = new SqlParameter("@SSN", SqlDbType.NVarChar, 10);
                    ssnParam.Value = entity.SSN;
                    command.Parameters.Add(ssnParam);

                    var nameParam = new SqlParameter("@MemberName", SqlDbType.NVarChar);
                    nameParam.Value = entity.Name;
                    command.Parameters.Add(nameParam);

                    var emailParam = new SqlParameter("@email", SqlDbType.NVarChar);
                    emailParam.Value = entity.Email;
                    command.Parameters.Add(emailParam);

                    var campusParam = new SqlParameter("@CampusAddress", SqlDbType.NVarChar);
                    campusParam.Value = entity.CampusAddress;
                    command.Parameters.Add(campusParam);

                    var homeParam = new SqlParameter("@HomeAddress", SqlDbType.NVarChar);
                    homeParam.Value = entity.HomeAddress;
                    command.Parameters.Add(homeParam);

                    var photoParam = new SqlParameter("@Photo", SqlDbType.VarBinary);
                    photoParam.Value = entity.Photo;
                    command.Parameters.Add(photoParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error handling.
                throw ex;
            }
        }

        public int Delete(Professor entity)
        {
            throw new NotImplementedException();
        }

        public Professor GetByInt(int id)
        {
            throw new NotImplementedException();
        }

        public Professor GetByString(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Professor entity)
        {
            throw new NotImplementedException();
        }
    }
}
