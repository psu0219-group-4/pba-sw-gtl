﻿using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories
{
    public class BookRepository : IBookRepository
    {
        public int Add(Book entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_CreateBook";

                    command.CommandType = CommandType.StoredProcedure;

                    var isbnParam = new SqlParameter("@ISBN", SqlDbType.NVarChar, 13);
                    isbnParam.Value = entity.ISBN;
                    command.Parameters.Add(isbnParam);

                    var titleParam = new SqlParameter("@Title", SqlDbType.NVarChar);
                    titleParam.Value = entity.Title;
                    command.Parameters.Add(titleParam);

                    var yearParam = new SqlParameter("@YearOfPublication", SqlDbType.Date);
                    yearParam.Value = entity.Year;
                    command.Parameters.Add(yearParam);

                    var editionParam = new SqlParameter("@Edition", SqlDbType.Int);
                    editionParam.Value = entity.Edition;
                    command.Parameters.Add(editionParam);

                    var rareParam = new SqlParameter("@Rare", SqlDbType.Bit);
                    rareParam.Value = entity.Rare;
                    command.Parameters.Add(rareParam);

                    var subjectParam = new SqlParameter("@SubjectArea", SqlDbType.NVarChar);
                    subjectParam.Value = entity.SubjectArea;
                    command.Parameters.Add(subjectParam);

                    var descriptionParam = new SqlParameter("@LongDescription", SqlDbType.NVarChar);
                    descriptionParam.Value = entity.Description;
                    command.Parameters.Add(descriptionParam);

                    var departmentParam = new SqlParameter("@DepartmentNo", SqlDbType.Int);
                    departmentParam.Value = entity.DepartmentNo;
                    command.Parameters.Add(departmentParam);

                    var authorParam = new SqlParameter("@AuthorId", SqlDbType.Int);
                    authorParam.Value = entity.AuthorId;
                    command.Parameters.Add(authorParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error handling.
                throw ex;
            }
        }

        public int Delete(Book entity)
        {
            throw new NotImplementedException();
        }

        public Book GetByInt(int id)
        {
            throw new NotImplementedException();
        }

        public Book GetByString(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Book entity)
        {
            throw new NotImplementedException();
        }
    }
}
