﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories.Interfaces
{
    public interface IRepository<T> where T : class
    {
        int Add(T entity);
        T GetByInt(int id);
        T GetByString(string id);
        void Update(T entity);
        int Delete(T entity);
    }
}
