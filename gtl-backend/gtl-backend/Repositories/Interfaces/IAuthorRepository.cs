﻿using gtl_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories.Interfaces
{
    public interface IAuthorRepository : IRepository<Author>
    {
    }
}
