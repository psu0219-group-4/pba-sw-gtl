﻿using gtl_backend.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;


namespace gtl_backend.Repositories
{
    public static class DbFactory
    {
        public static DbConnection CreateDbConnection()
        {
            string providerName = AppSettingsProvider.ProviderName; // Get from appsettings.json
            string connectionString = AppSettingsProvider.ConnectionString; // Get from appsettings.json

            // Assuming error
            DbConnection connection = null;

            if (connectionString != null)
            {
                try
                {
                    DbProviderFactory factory = DbProviderFactories.GetFactory(providerName);
                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
                catch (Exception ex)
                {
                    if (connection != null)
                    {
                        connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }

            return connection;
        }
    }
}
