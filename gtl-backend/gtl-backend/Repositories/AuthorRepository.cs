﻿using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        public int Add(Author entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_CreateBookCopy";

                    command.CommandType = CommandType.StoredProcedure;

                    var firstNameParam = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                    firstNameParam.Value = entity.FirstName;
                    command.Parameters.Add(firstNameParam);

                    var lastNameParam = new SqlParameter("@LastName", SqlDbType.NVarChar);
                    lastNameParam.Value = entity.LastName;
                    command.Parameters.Add(lastNameParam);

                    var birthParam = new SqlParameter("@YearOfBirth", SqlDbType.Date);
                    birthParam.Value = entity.YearOfBirth;
                    command.Parameters.Add(birthParam);

                    var deathParam = new SqlParameter("@YearOfDeath", SqlDbType.Date);
                    deathParam.Value = entity.YearOfDeath;
                    command.Parameters.Add(deathParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error handling.
                throw ex;
            }
        }

        public int Delete(Author entity)
        {
            throw new NotImplementedException();
        }

        public Author GetByInt(int id)
        {
            throw new NotImplementedException();
        }

        public Author GetByString(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(Author entity)
        {
            throw new NotImplementedException();
        }
    }
}
