﻿using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Repositories
{
    public class BookCopyRepository : IBookCopyRepository
    {
        public int Add(BookCopy entity)
        {
            try
            {
                using (DbConnection connection = DbFactory.CreateDbConnection())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = "PROC_CreateBookCopy";

                    command.CommandType = CommandType.StoredProcedure;

                    var isbnParam = new SqlParameter("@ISBN", SqlDbType.NVarChar, 13);
                    isbnParam.Value = entity.ISBN;
                    command.Parameters.Add(isbnParam);

                    int affectedRows = command.ExecuteNonQuery();
                    return affectedRows;
                }
            }
            catch (Exception ex)
            {
                // TODO: Error handling.
                throw ex;
            }
        }

        public int Delete(BookCopy entity)
        {
            throw new NotImplementedException();
        }

        public BookCopy GetByInt(int id)
        {
            throw new NotImplementedException();
        }

        public BookCopy GetByString(string id)
        {
            throw new NotImplementedException();
        }

        public void Update(BookCopy entity)
        {
            throw new NotImplementedException();
        }
    }
}
