﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers.Interfaces
{
    public interface IAuthorManager
    {
        int CreateAuthor(string firstName, string lastName, DateTime yearOfBirth, DateTime yearOfDeath);
    }
}
