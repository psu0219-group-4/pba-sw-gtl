﻿using gtl_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers.Interfaces
{
    public interface IMemberManager
    {
        Member GetMember(string ssn);
        Student CreateStudent(string ssn, string name, string email, string campusAddress, string homeAddress, string photoPath, string phoneNumber);
        Professor CreateProfessor(string ssn, string name, string email, string campusAddress, string homeAddress, string photoPath, string phoneNumber);
    }
}
