﻿using System;
using gtl_backend.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers.Interfaces
{
    public interface IBookManager
    {
        int CreateBook(string isbn, string title, DateTime yearOfPublication, int edition, bool rare, string subjectArea, string description, int departmentNo, int authorId);
    }
}
