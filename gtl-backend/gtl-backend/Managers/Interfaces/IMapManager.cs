﻿using gtl_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers.Interfaces
{
    public interface IMapManager
    {
        int CreateMap(string isbn, string title, DateTime yearOfPublication, int edition, bool rare, string description, string area, int departmentNo);
    }
}
