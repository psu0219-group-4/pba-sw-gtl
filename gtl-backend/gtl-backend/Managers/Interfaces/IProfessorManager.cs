﻿using gtl_backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers.Interfaces
{
    public interface IProfessorManager
    {
        int CreateProfessor(string ssn, string memberName, string email, string campusAddress, string homeAddress, byte[] photo);
    }
}
