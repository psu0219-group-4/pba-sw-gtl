﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Models;

namespace gtl_backend.Managers.Interfaces
{
    public interface ILoanManager
    {
        int LoanBook(int bookCopyId, string ssn);
        int ReturnBook(int bookCopyId, string ssn);
    }
} 
