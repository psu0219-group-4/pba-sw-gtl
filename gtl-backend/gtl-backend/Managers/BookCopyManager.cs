﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class BookCopyManager : IBookCopyManager
    {
        private readonly IBookCopyRepository _bookCopyRepository;
        public BookCopyManager(IBookCopyRepository bookCopyRepository)
        {
            this._bookCopyRepository = bookCopyRepository;
        }
        public int CreateBookCopy(string isbn)
        {
            var bookCopy = new BookCopy
            {
                ISBN = isbn
            };

            return this._bookCopyRepository.Add(bookCopy);
        }
    }
}
