﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class ProfessorManager : IProfessorManager
    {
        private readonly IProfessorRepository _professorRepository;
        public ProfessorManager(IProfessorRepository professorRepository)
        {
            this._professorRepository = professorRepository;
        }

        public int CreateProfessor(string ssn, string memberName, string email, string campusAddress, string homeAddress, byte[] photo)
        {
            var professor = new Professor
            {
                SSN = ssn,
                Name = memberName,
                Email = email,
                CampusAddress = campusAddress,
                HomeAddress = homeAddress,
                Photo = photo
            };

            return this._professorRepository.Add(professor);
        }
    }
}
