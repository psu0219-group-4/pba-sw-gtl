﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class BookManager : IBookManager
    {
        private readonly IBookRepository _bookRepository;
        public BookManager(IBookRepository bookRepository)
        {
            this._bookRepository = bookRepository;
        }
        public int CreateBook(string isbn, string title, DateTime yearOfPublication, int edition, bool rare, string subjectArea, string description, int departmentNo, int authorId)
        {
            var book = new Book
            {
                ISBN = isbn,
                Title = title,
                Year = yearOfPublication,
                Edition = edition,
                Rare = rare,
                SubjectArea = subjectArea,
                Description = description,
                DepartmentNo = departmentNo,
                AuthorId = authorId
            };

            return this._bookRepository.Add(book);
        }
    }
}
