﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class LoanManager : ILoanManager
    {
        private readonly ILoanRepository _loanRepository;
        public LoanManager(ILoanRepository loanRepository)
        {
            this._loanRepository = loanRepository;
        }

        public int LoanBook(int bookCopyId, string ssn)
        {
            var loan = new Loan
            {
                MemberSsn = ssn,
                BookId = bookCopyId
            };

            return _loanRepository.Add(loan);
        }

        public int ReturnBook(int bookCopyId, string ssn)
        {
            var loan = new Loan
            {
                MemberSsn = ssn,
                BookId = bookCopyId
            };

            return this._loanRepository.Delete(loan);
        }
    }
}
