﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class MapManager : IMapManager
    {
        private readonly IMapRepository _mapRepository;
        public MapManager(IMapRepository mapRepository)
        {
            this._mapRepository = mapRepository;
        }
        public int CreateMap(string isbn, string title, DateTime yearOfPublication, int edition, bool rare, string description, string area, int departmentNo)
        {
            var map = new Map
            {
                ISBN = isbn,
                Title = title,
                Year = yearOfPublication,
                Edition = edition,
                Rare = rare,
                Description = description,
                Area = area,
                DepartmentNo = departmentNo
            };

            return this._mapRepository.Add(map);
        }
    }
}
