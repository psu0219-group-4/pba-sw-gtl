﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class AuthorManager : IAuthorManager
    {
        private readonly IAuthorRepository _authorRepository;
        public AuthorManager(IAuthorRepository authorRepository)
        {
            this._authorRepository = authorRepository;
        }

        public int CreateAuthor(string firstName, string lastName, DateTime yearOfBirth, DateTime yearOfDeath)
        {
            var author = new Author
            {
                FirstName = firstName,
                LastName = lastName,
                YearOfBirth = yearOfBirth,
                YearOfDeath = yearOfDeath
            };

            return this._authorRepository.Add(author);
        }
    }
}
