﻿using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using gtl_backend.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Managers
{
    public class StudentManager : IStudentManager
    {
        private readonly IStudentRepository _studentRepository;
        public StudentManager(IStudentRepository studentRepository)
        {
            this._studentRepository = studentRepository;
        }

        public int CreateStudent(string ssn, string memberName, string email, string campusAddress, string homeAddress, byte[] photo)
        {
            var student = new Student
            {
                SSN = ssn,
                Name = memberName,
                Email = email,
                CampusAddress = campusAddress,
                HomeAddress = homeAddress,
                Photo = photo
            };

            return this._studentRepository.Add(student);
        }
    }
}
