﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public class Map : LibraryItem
    {
        public string Area { get; set; }
        public string Description { get; set; }
    }
}
