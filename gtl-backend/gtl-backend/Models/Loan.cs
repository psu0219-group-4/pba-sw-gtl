﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public class Loan
    {
        public DateTime StartDate { get; set; }
        public bool Returned { get; set; }
        public string MemberSsn { get; set; }
        public Member Member { get; set; }
        public int BookId { get; set; }
        public BookCopy Book { get; set; }
    }
}
