﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public class Book : LibraryItem
    {
        public string SubjectArea { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
        public List<Author> WrittenBy { get; set; }
        public List<BookCopy> Copies { get; set; }

    }
}
