﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public abstract class LibraryItem
    {
        public int DepartmentNo { get; set; }
        public bool Rare { get; set; }
        public string Title { get; set; }
        public DateTime Year { get; set; }
        public int Edition { get; set; }
        public string ISBN { get; set; }
        public string Type { get; set; }
    }
}
