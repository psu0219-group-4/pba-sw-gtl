﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public class Professor : Member
    {
        public List<string> Courses { get; set; }
    }
}
