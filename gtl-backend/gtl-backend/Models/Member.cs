﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public abstract class Member
    {
        public string SSN { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CampusAddress { get; set; }
        public string HomeAddress { get; set; }
        public List<string> PhoneNumber { get; set; }
        public byte[] Photo { get; set; }
    }
}
