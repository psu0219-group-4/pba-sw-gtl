﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public static class AppSettingsProvider
    {
        public static string ConnectionString { get; set; }
        public static string ProviderName { get; set; }
    }
}
