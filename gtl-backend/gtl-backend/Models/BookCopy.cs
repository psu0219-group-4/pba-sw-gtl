﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Models
{
    public class BookCopy
    {
        public int Id { get; set; }
        public string ISBN { get; set; }
        public Book Book { get; set; }
    }
}
