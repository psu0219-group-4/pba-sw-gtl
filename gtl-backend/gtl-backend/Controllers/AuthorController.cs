﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorManager _authorManager;
        public AuthorController(IAuthorManager authorManager)
        {
            this._authorManager = authorManager;
        }

        [HttpPost]
        public ActionResult CreateAuthor([FromForm]string firstName, [FromForm]string lastName, [FromForm]DateTime yearOfBirth, [FromForm]DateTime yearOfDeath)
        {
            try
            {
                int affectedRows = this._authorManager.CreateAuthor(firstName, lastName, yearOfBirth, yearOfDeath);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}