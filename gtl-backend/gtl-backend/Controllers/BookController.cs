﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookManager _bookManager;
        public BookController(IBookManager bookManager)
        {
            this._bookManager = bookManager;
        }

        [HttpPost]
        public ActionResult CreateBook([FromForm]string isbn, [FromForm]string title, [FromForm]DateTime yearOfPublication, [FromForm]int edition, [FromForm]bool rare, [FromForm]string subjectArea, [FromForm]string longDescription, [FromForm]int departmentNo, [FromForm]int authorId)
        {
            try
            {
                int affectedRows = this._bookManager.CreateBook(isbn, title, yearOfPublication, edition, rare, subjectArea, longDescription, departmentNo, authorId);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}