﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers.Interfaces;
using gtl_backend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly ILoanManager _loanManager;
        public LoanController(ILoanManager loanManager)
        {
            this._loanManager = loanManager;
        }

        [HttpPost]
        [Route("api/[controller]/loan")]
        public ActionResult CreateLoan([FromForm]int id, [FromForm]string ssn)
        {
            if (id < 0) throw new ArgumentOutOfRangeException(nameof(id));
            if (string.IsNullOrEmpty(ssn)) throw new ArgumentNullException(nameof(ssn));

            try
            {
                int affectedRows = this._loanManager.LoanBook(id, ssn);
                if(affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/[controller]/return")]
        public ActionResult ReturnBook([FromForm]int id, [FromForm]string ssn)
        {
            try
            {
                int affectedRows = this._loanManager.ReturnBook(id, ssn);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}