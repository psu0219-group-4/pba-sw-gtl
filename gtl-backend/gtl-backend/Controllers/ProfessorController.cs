﻿using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {
        private readonly IProfessorManager _professorManager;
        public ProfessorController(IProfessorManager professorManager)
        {
            this._professorManager = professorManager;
        }

        [HttpPost]
        public ActionResult CreateStudent([FromForm]string ssn, [FromForm]string memberName, [FromForm]string email, [FromForm]string campusAddress, [FromForm]string homeAddress, [FromForm]byte[] photo)
        {
            try
            {
                int affectedRows = this._professorManager.CreateProfessor(ssn, memberName, email, campusAddress, homeAddress, photo);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
