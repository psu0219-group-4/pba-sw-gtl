﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookCopyController : ControllerBase
    {
        private readonly IBookCopyManager _bookCopyManager;
        public BookCopyController(IBookCopyManager bookCopyManager)
        {
            this._bookCopyManager = bookCopyManager;
        }

        [HttpPost]
        public ActionResult CreateBookCopy([FromForm]string isbn)
        {
            int affectedRows = this._bookCopyManager.CreateBookCopy(isbn);
            if (affectedRows < 1) return BadRequest("Error!");

            return Ok();
        }
    }
}