﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapController : ControllerBase
    {
        private readonly IMapManager _mapManager;
        public MapController(IMapManager mapManager)
        {
            this._mapManager = mapManager;
        }

        [HttpPost]
        public ActionResult CreateMap([FromForm]string isbn, [FromForm]string title, [FromForm]DateTime yearOfPublication, [FromForm]int edition, [FromForm]bool rare, [FromForm]string longDescription, [FromForm]string area, [FromForm]int departmentNo)
        {
            try
            {
                int affectedRows = this._mapManager.CreateMap(isbn, title, yearOfPublication, edition, rare, longDescription, area, departmentNo);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}