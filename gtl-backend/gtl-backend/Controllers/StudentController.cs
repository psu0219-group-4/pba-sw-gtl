﻿using gtl_backend.Managers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gtl_backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentManager _studentManager;
        public StudentController(IStudentManager studentManager)
        {
            this._studentManager = studentManager;
        }

        [HttpPost]
        public ActionResult CreateStudent([FromForm]string ssn, [FromForm]string memberName, [FromForm]string email, [FromForm]string campusAddress, [FromForm]string homeAddress, [FromForm]byte[] photo)
        {
            try
            {
                int affectedRows = this._studentManager.CreateStudent(ssn, memberName, email, campusAddress, homeAddress, photo);
                if (affectedRows < 1) return BadRequest("Error!");

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
