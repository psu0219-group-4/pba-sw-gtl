--drop database GTL;
--go
--create database GTL;
go
use dmaa0916_175324;
go

-- EXEC sp_MSforeachtable @command1 = "DROP TABLE ?"

create table Catalogue
(
	DepartmentNo int not null,
	Department nvarchar(max) not null,
	primary key (DepartmentNo)
);

create table Member
(
	SSN nvarchar(10) not null,
	-- should be limited
	MemberName nvarchar(max) not null,
	Email nvarchar(max) not null,
	CampusAddress nvarchar(max) not null,
	HomeAddress nvarchar(max) not null,
	Photo varbinary(max) not null,
	-- max kb
	primary key (SSN)
);

create table Author
(
	AuthorId int IDENTITY(1,1) not null,
	FirstName nvarchar(max) not null,
	LastName nvarchar(max) not null,
	YearOfBirth Date not null,
	YearOfDeath Date,
	primary key (AuthorId)
);

create table LibraryItem
(
	ISBN nvarchar(13) not null,
	Title nvarchar(max) not null,
	YearOfPublication Date not null,
	Edition int not null,
	Rare bit not null,
	SubjectArea nvarchar(max),
	LongDescription nvarchar(max),
	Area nvarchar(max),
	ItemType nvarchar(max) not null,
	DepartmentNo int not null,
	primary key(ISBN),
	foreign key(DepartmentNo) references Catalogue(DepartmentNo)
);

create table Student
(
	SSN nvarchar(10) not null,
	-- should be limited
	Class nvarchar(max) not null,
	primary key(SSN),
	foreign key(SSN) references Member(SSN)
);

create table Professor
(
	SSN nvarchar(10) not null,
	-- should be limited
	primary key(SSN),
	foreign key(SSN) references Member(SSN)
);

create table PhoneNumber
(
	SSN nvarchar(10) not null,
	-- should be limited
	PhoneNumber nvarchar(8) not null,
	primary key(SSN, PhoneNumber),
	foreign key(SSN) references Member(SSN)
);

create table Course
(
	SSN nvarchar(10) not null,
	-- should be limited
	Course nvarchar(10) not null,
	primary key(SSN, Course),
	foreign key(SSN) references Member(SSN)
);

create table BookCopy
(
	BookCopyId int IDENTITY(1,1) not null,
	ISBN nvarchar(13) not null,
	primary key(BookCopyId),
	foreign key(ISBN) references LibraryItem(ISBN)
);

create table WrittenBy
(
	AuthorId int not null,
	ISBN nvarchar(13) not null,
	primary key(AuthorId, ISBN),
	foreign key(ISBN) references LibraryItem(ISBN),
	foreign key(AuthorId) references Author(AuthorId)
);

create table Loans
(
	SSN nvarchar(10) not null,
	-- should be limited
	BookCopyId int not null,
	StartDate Date not null,
	Returned bit not null,
	primary key(SSN, BookCopyId, StartDate),
	foreign key(SSN) references Member(SSN),
	foreign key(BookCopyId) references BookCopy(BookCopyId),
);

create index idx_returned on Loans (Returned); 

go

create trigger TRIG_NoDuplicateLoans on Loans 
after insert
as

	declare @bookCopy int;
	declare @n int

	set @bookCopy = (select BookCopyId
from inserted) -- This will fail on bulk inserts as inserted will be a row of all the bulk inserted values
	set @n = (select count(*)
from Loans
where BookCopyId = @bookCopy and Returned = 0) - 1 -- here it is inserted so substract 1

	if(@n > 0)
	BEGIN
	raiserror('Error! The book is already loaned out.', 16,1);
	rollback tran;
END

go

create function FN_NoOfLoans(@SSN nvarchar(10))  -- nvarchar should be limited
returns table
as
return(
	select count(*) as NoOfLoans
from Loans
	join Member on Loans.SSN = Member.SSN
where Loans.Returned = 0 and Loans.SSN = @SSN
);

go

create function FN_AvaileableBookCopies(@ISBN nvarchar(13))
returns table
as
return(
	-- total number of compies of a book with a specific isbn
	-- then substract the number of active loans of that book
	select (select count(*) NoCopies
	from BookCopy
	where @ISBN = BookCopy.ISBN) - count(*) as AvailableCopies
from Loans
	join BookCopy on Loans.BookCopyId = BookCopy.BookCopyId
where Loans.Returned = 0 and BookCopy.ISBN = @ISBN
);

go

create trigger TRIG_MaxNoOfLoans on Loans 
after insert
as
	declare @ssn nvarchar(10) -- should be limited
	DECLARE @noOfLoans int	
begin
	set @ssn = (select SSN
	from inserted)
	SET @noOfLoans = (select NoOfLoans
	from FN_NoOfLoans(@ssn))
	if(@noOfLoans > 5)
	begin
		raiserror('Error! The member already have 5 loans.', 16,1)
		rollback tran
	end
end

go

CREATE PROCEDURE PROC_CreateLoan
	@SSN NVARCHAR(10),
	@BookCopyId int
AS
INSERT INTO Loans
VALUES
	(@SSN, @BookCopyId, getutcdate(), 0)
RETURN

go

CREATE PROCEDURE PROC_ReturnLoan
	@SSN NVARCHAR(10),
	@BookCopyId int
AS
UPDATE Loans
SET Returned = 1
WHERE SSN = @SSN and BookCopyId = @BookCopyId

RETURN

go

CREATE PROCEDURE PROC_CreateStudent
	@SSN nvarchar(10),
	@MemberName nvarchar(max),
	@Email nvarchar(max),
	@CampusAddress nvarchar(max),
	@HomeAddress nvarchar(max),
	@Photo varbinary(max),

	@Class nvarchar(max)
AS
BEGIN TRAN
INSERT INTO Member
VALUES(@SSN, @MemberName, @Email, @CampusAddress, @HomeAddress, @Photo)
INSERT INTO Student
VALUES(@SSN, @Class)
COMMIT
RETURN

go

CREATE PROCEDURE PROC_CreateProfessor
	@SSN nvarchar(10),
	@MemberName nvarchar(max),
	@Email nvarchar(max),
	@CampusAddress nvarchar(max),
	@HomeAddress nvarchar(max),
	@Photo varbinary(max)
AS
BEGIN TRAN
INSERT INTO Member
VALUES(@SSN, @MemberName, @Email, @CampusAddress, @HomeAddress, @Photo)

INSERT INTO Professor
VALUES(@SSN)

COMMIT
RETURN

go

CREATE PROCEDURE PROC_AttachAuthorToBook
	@AuthorId int,
	@ISBN nvarchar(13)
AS
INSERT INTO WrittenBy
VALUES
	(@AuthorId, @ISBN)
RETURN

GO


CREATE PROCEDURE PROC_CreateBook
	@ISBN nvarchar(13),
	@Title nvarchar(max),
	@YearOfPublication Date,
	@Edition int,
	@Rare bit,
	@SubjectArea nvarchar(max),
	@LongDescription nvarchar(max),
	-- @Area nvarchar(max),
	-- @ItemType nvarchar(max),
	@DepartmentNo int,

	@AuthorId int
AS
BEGIN TRAN

INSERT INTO LibraryItem
VALUES
	(@ISBN, @Title, @YearOfPublication, @Edition, @Rare, @SubjectArea, @LongDescription, NULL, 'Book', @DepartmentNo)

EXEC PROC_AttachAuthorToBook @AuthorId, @ISBN;

COMMIT

RETURN

go

CREATE PROCEDURE PROC_CreateMap
	@ISBN nvarchar(13),
	@Title nvarchar(max),
	@YearOfPublication Date,
	@Edition int,
	@Rare bit,
	-- @SubjectArea nvarchar(max),
	@LongDescription nvarchar(max),
	@Area nvarchar(max),
	-- @ItemType nvarchar(max),
	@DepartmentNo int

AS

INSERT INTO LibraryItem
VALUES
	(@ISBN, @Title, @YearOfPublication, @Edition, @Rare, NULL, @LongDescription, @Area, 'Map', @DepartmentNo)

RETURN

go



CREATE PROCEDURE PROC_CreateBookCopy
	@ISBN nvarchar(13)
AS
INSERT INTO BookCopy
	(ISBN)
VALUES(@ISBN)
RETURN

go

CREATE PROCEDURE PROC_CreateAuthor
	-- AuthorId int IDENTITY(1,1) not null,
	@FirstName nvarchar(max),
	@LastName nvarchar(max),
	@YearOfBirth Date,
	@YearOfDeath Date
AS
INSERT INTO Author
	(FirstName, LastName, YearOfBirth, YearOfDeath)
VALUES(@FirstName, @LastName, @YearOfBirth, @YearOfDeath)
RETURN

GO

-- create function FN_NoOfLoans(@SSN nvarchar(10))  -- nvarchar should be limited
-- returns table
-- as
-- return(
-- 	select count(*) as NoOfLoans
-- from Loans
-- 	join Member on Loans.SSN = Member.SSN
-- where Loans.Returned = 0 and Loans.SSN = @SSN
-- );

CREATE FUNCTION FN_OverDueStudentLoans()
RETURNS table
AS
RETURN(
	SELECT Loans.*
--Loans.SSN, Loans.BookCopyId, Loans.StartDate, Loans.Returned
FROM Loans
	JOIN Student on Loans.SSN = Student.SSN
WHERE Loans.Returned = 0 and DATEDIFF(day, Loans.StartDate, getutcdate()) > 21
);

GO

CREATE FUNCTION FN_OverDueProfessorLoans()
RETURNS table
AS
RETURN(
	SELECT Loans.*
--Loans.SSN, Loans.BookCopyId, Loans.StartDate, Loans.Returned
FROM Loans
	JOIN Professor on Loans.SSN = Professor.SSN
WHERE Loans.Returned = 0 and DATEDIFF(day, Loans.StartDate, getutcdate()) > ((365/12)*3) -- 3 months
);

GO

CREATE FUNCTION FN_OverDueLoans()
RETURNS table
AS
RETURN(	
					SELECT *
	FROM FN_OverDueStudentLoans()
UNION
	SELECT *
	FROM FN_OverDueProfessorLoans()

)

GO

CREATE VIEW AnonLoans
AS
	SELECT Loans.BookCopyId, Loans.StartDate, Loans.Returned
	FROM Loans

GO

CREATE FUNCTION FN_ReadStudent(@SSN nvarchar(10))
RETURNS table
AS
RETURN (
	SELECT Member.*, Student.Class, PhoneNumber.PhoneNumber
FROM Student
	JOIN Member on Student.SSN = Member.SSN
	JOIN PhoneNumber on Student.SSN = PhoneNumber.SSN
WHERE Student.SSN = @SSN
)

GO


CREATE FUNCTION FN_ReadProfessor(@SSN nvarchar(10))
RETURNS table
AS
RETURN (
	SELECT Member.*, PhoneNumber.PhoneNumber, Course.Course
FROM Professor
	JOIN Member on Professor.SSN = Member.SSN
	JOIN PhoneNumber on Professor.SSN = PhoneNumber.SSN
	JOIN Course on Professor.SSN = Course.SSN
WHERE Professor.SSN = @SSN
)

GO

CREATE FUNCTION FN_ReadBook(@ISBN NVARCHAR(13))
RETURNS table
as
RETURN (
	SELECT li.ISBN, li.Title, li.YearOfPublication, li.Edition, li.Rare, li.SubjectArea, li.LongDescription, li.DepartmentNo,
	a.FirstName as AuthorFirstName, a.LastName as AuthorLastName, a.YearOfBirth as AuthorYearOfBirth, a.YearOfDeath as AuthorYearOfDeath
FROM LibraryItem as li
	JOIN WrittenBy on WrittenBy.ISBN = li.ISBN
	JOIN Author as a on WrittenBy.AuthorId = a.AuthorId
WHERE li.ISBN = @ISBN and li.ItemType = 'Book'
)

GO

 CREATE FUNCTION FN_CheckIfRareItem(@BookCopyId int)
 RETURNS table
 AS
 RETURN(select BookCopy.*, LibraryItem.Rare FROM BookCopy JOIN LibraryItem on BookCopy.ISBN
 = LibraryItem.ISBN where BookCopy.BookCopyId = @BookCopyId and Rare = 1
 );

 GO

 create trigger TRIG_CantLoanRareItem on Loans
after insert
as
	DECLARE @BookCopyId int
	DECLARE @noOfRareItems int
begin
	set @BookCopyId = (select BookCopyId from inserted)
	set @noOfRareItems = (select count(*) from FN_CheckIfRareItem(@BookCopyId))
	if(@noOfRareItems >= 1)
	begin
		raiserror('Error! This item is rare', 16,1)
		rollback  tran
	end
end


GO

CREATE PROCEDURE PROC_AddPhoneNumberToMember
	@SSN nvarchar(10),
	@PhoneNumber nvarchar(8)
AS
INSERT INTO PhoneNumber
VALUES
	(@SSN, @PhoneNumber)
RETURN

GO
